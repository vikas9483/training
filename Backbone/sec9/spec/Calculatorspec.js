describe("Calculator",function(){
    var calculator;
    beforeEach(function(){
        calculator = new  Calculator();
    });
    afterEach(function(){

    });
    describe("add",function(){
        it("able to add 2 number",function(){
            var result = calculator.add(2,3);
            expect(result).toEqual(5);
        });
        it("throw an error if both argument are not provided",function(){
            expect(function(){
                calculator.add(1);
            }).toThrow();
        });
    it("should be called with the right argument",function(){
        spyOn(calculator,"add").and.throwError("someError");
        expect(function(){
            var result = calculator.add(2,5);
        }).toThrowError("someError");

        });
    });
});