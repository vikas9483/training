var Calculator = function(){
    var add = function(a,b){
        if(!a || !b)
            throw new Error("Add method expect two argument")
            
        return a+b;
    }
    return {
    add :add
    }
}
