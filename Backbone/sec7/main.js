var ArtistView = Backbone.View.extend({
    render:function(){
        this.$el.html("ARTIST VIEW");
        return this;
    }
});

var AlbumsView = Backbone.View.extend({
    render:function(){
        this.$el.html("ALBUMS VIEW");
        return this;
    }
});

var GeneresView = Backbone.View.extend({
    render:function(){
        this.$el.html("GENERES VIEW");
        return this;
    }
});
var AppRouter = Backbone.Router.extend({
    routers:{
        "albums":"viewAlbums",
        "albums/:albumID":"viewAlbumById",
        "artists:"viewArtists"
    },
    viewAlbumById:function(albumId){

    },
    viewAlbums :function(){
        var view = new AlbumsView({el:"#container"});
    }
});